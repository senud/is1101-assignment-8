#include<stdio.h>
#define MAX 20
struct student
{
    char first_name[MAX];
    char sub[MAX];
    float marks;
};
int main()
{
    int i=0,num;
    printf("Enter the number of students in the class: ");
    scanf("%d",&num);

    struct student studentdetails[num];

    for (i=0;i<num; i++){

    printf("\n\nEnter the information about student %d:\n\n",i+1);

    printf("Enter first name: ");
    scanf("%s",studentdetails[i].first_name);
    printf("Enter subject: ");
    scanf("%s",studentdetails[i].sub);
    printf("Enter marks: ");
    scanf("%f",&studentdetails[i].marks);
    }
    printf("\n");
    printf("\nDisplay Information:-\n");
    for(i=0;i<num;i++){

        printf("\n\nDisplay information about student %d\n",i+1);
        printf("\nFirst Name:%s",studentdetails[i].first_name);
        printf("\nSubject:%s",studentdetails[i].sub);
        printf("\nMarks:%.1f\n",studentdetails[i].marks);
    }
    return 0;
}


